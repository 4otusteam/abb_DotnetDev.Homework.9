﻿namespace DotnetDev.Homework._9
{
    public class NumberMethods : INumberMethods
    {
        private static readonly INumberMethods instance = new ExtraNumberMethods();

        private int _myNumber { get; set; }
        private ISettings _appSettings { get; set; }

        public static INumberMethods GetInstance()
        {
            return instance;
        }

        public NumberMethods() 

        {
            Random rand = new Random();
            _appSettings = new Settings();
            _myNumber = rand.Next(_appSettings.MinValue, _appSettings.MaxValue);
        }

        public int MyNumber
        {
            get { return _myNumber; }
        }

        //Update the set value
        public Boolean SetNewNumber()
        {
            Random rand = new Random();
            _myNumber = rand.Next(_appSettings.MinValue, _appSettings.MaxValue);
            return true;
        }

        //Answers for resutl
        public enum ResultType
        {
            Less,
            Equal,
            Greater
        }

        public virtual string CompareResult(int x)
        {
            if (x < _myNumber) return ResultType.Less.ToString();
            if (x > _myNumber) return ResultType.Greater.ToString();
            return ResultType.Equal.ToString();
        }

    }
}
