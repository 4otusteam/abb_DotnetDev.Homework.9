﻿namespace DotnetDev.Homework._9
{
    public class Attempts : IAttempts
    {

        private static  Attempts instance = new Attempts();
        private readonly ISettings _settings = Settings.GetInstance();

        public int attempts { get; set; }
        public int currentAttempt { get; set; }

        
        public void Start()
        {
            attempts = _settings.GetMaxAttempts();
            currentAttempt = attempts;

        }
        public static IAttempts GetInstance() { return instance;    }
        public int GetAttempt() 
        {
            return currentAttempt;
        }
        public void DecreaseAttempt()
        {
            currentAttempt--;
        }

    }
}
