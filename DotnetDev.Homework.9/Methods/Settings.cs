﻿namespace DotnetDev.Homework._9
{
    public class Settings : ISettings
    {
        private static readonly ISettings instance = new Settings();
        int _maxAttempts { get; set; }
        public int MinValue { get; set; }
        public int MaxValue { get; set; }

        //Singletone to avoid creating new objects
        public static ISettings GetInstance()
        {
            return instance;
        }

        public Settings() 
        {
            MinValue = 1;
            MaxValue = 100;
        }

        public void SetMaxAttmepts(int x) {
            _maxAttempts = x;
        }
        public int GetMaxAttempts() {
            return _maxAttempts;
        }


    }
}

