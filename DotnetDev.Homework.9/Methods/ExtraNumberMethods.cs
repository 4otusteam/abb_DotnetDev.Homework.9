﻿
namespace DotnetDev.Homework._9
{
    public class ExtraNumberMethods : NumberMethods
    {

        public string theWinner { get; set; }

        public enum ResultType
        {
            MinusZero,
            TooSmall,
            TooMuch,
            AbsEqual
        }

        public override String  CompareResult(int x)
        {
            if (x < 0) return "Negative";
            if (x < MyNumber) return ResultType.TooSmall.ToString();
            if (x > MyNumber) return ResultType.TooMuch.ToString();
            
            return ResultType.AbsEqual.ToString();
        }
        public string YouAreTheWinner()
        {
            theWinner = "You are the winner! Congrats!";
            return theWinner;
        }

    }
    
}
