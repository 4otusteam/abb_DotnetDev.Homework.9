﻿using Microsoft.AspNetCore.Mvc;
using static DotnetDev.Homework._9.NumberMethods;

namespace DotnetDev.Homework._9.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [Produces("application/json")]

    public class NumberController : ControllerBase
    {

        //NumberMethods is a class for the work with setting a random number. Singleton is to use only one instance and 
        //not to create a new number on each step
        INumberMethods numberMethods = ExtraNumberMethods.GetInstance();//.GetInstance();
        ISettings settings = Settings.GetInstance();//.GetInstance();
        IAttempts attempts = Attempts.GetInstance();

        

        [HttpPost("SetAttemps")]
        public IActionResult SetAttempts(int x)
        {
            settings.SetMaxAttmepts(x);
            attempts.Start();
            //ss.SetMaxAttemps(x);
            return  Ok(x);
        }

        [HttpGet("GetNumber")]
        public string GetNumber ()
        {
            return "my number is: " + numberMethods.MyNumber + ", MaxAttempts: " + settings.GetMaxAttempts().ToString();
        }

        [HttpPost("CompareNumber")]
        public String CompareNumber(int x)
        {
            String resultType = numberMethods.CompareResult(x);
            attempts.DecreaseAttempt();
            int currentAttempt = attempts.GetAttempt();

            string messText = numberMethods.GetType().Name;


            if (resultType == "Negative")
            {
                return "How could you do that??";
            }

            if (resultType == ResultType.Equal.ToString())
            {
                if(numberMethods.GetType().GetProperties().Where(p=>p.Name == "theWinner").Any())
                {
                    messText += "You're the winner!";
                }
                bool isNewNumber = numberMethods.SetNewNumber();

                return $"{messText} Equal!the number is '{x}'. The winner! you still have {currentAttempt} attempts to got the new Value!";
            }
            if (currentAttempt == settings.GetMaxAttempts())
            {
                messText = "{messText} There's only 1 left attempt";
            }
            else if (currentAttempt < settings.GetMaxAttempts())
            {
                messText = $"{messText} You have left {currentAttempt} of {  settings.GetMaxAttempts()} attempts";
            }

            if (currentAttempt < 1)
            {
                numberMethods = null;
                return "Game over, no left attempts.";
            }


         

            return resultType.ToString() + ". " +  messText;
        }
    }
}
