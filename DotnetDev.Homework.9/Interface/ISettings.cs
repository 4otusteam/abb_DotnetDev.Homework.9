﻿namespace DotnetDev.Homework._9
{
    public interface ISettings
    {
        //public int Attempts { get; set; }
        public int MinValue { get; set; }
        public int MaxValue { get; set; }
        public void SetMaxAttmepts(int x);
        public int GetMaxAttempts();

    }
}
