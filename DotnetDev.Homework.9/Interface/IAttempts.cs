﻿namespace DotnetDev.Homework._9
{
    public interface IAttempts
    {
        public void Start();
        public int attempts { get; set; }
        public int GetAttempt();
        public void DecreaseAttempt();

    }
}
